def factorial(n):
    if n >= 2:
        return n*factorial(n-1)
    else:
        return 1

def check_sum_factorials(n):
    n_list = [int(i) for i in str(n)]
    n_list = sorted(n_list)[-1::-1]
    s = 0
    for i in n_list:
        s += factorial(i)
        if s > n:
            return None

    if s == n:
        return n

assert factorial(0) == 1
assert factorial(1) == 1
assert factorial(6) == 720
assert check_sum_factorials(145) is not None

s = 0
for i in range(3, 100000):
    n = check_sum_factorials(i)
    if n is not None:
        print(n)
        s += n

print(s)
