from tools import is_pandigital, is_prime


def number_to_list(num):
    number_list = []
    while True:
        if num == 0:
            break
        new_num = num // 10
        number_list.append(num - new_num * 10)
        num = new_num

    return number_list[::-1]


def list_to_number(list_num):
    num = 0
    n = len(list_num)
    for i, el in enumerate(list_num):
        num += el * 10**(n - i - 1)

    return num


def swap(l, i, j):
    l_new = l.copy()
    l_new[i], l_new[j] = l_new[j], l_new[i]
    return l_new


def permutation(list_num, ix=0):
    if ix == len(list_num) - 1:
        yield list_num

    for i in range(ix, len(list_num)):
        if i != ix:
            list_num_new = swap(list_num, ix, i)
        else:
            list_num_new = list_num.copy()
        for p in permutation(list_num_new, ix + 1):
            yield p


# largest pandigital: 987654321
assert(is_pandigital(987654321))

nums = [123456789, 12345678, 1234567,
        123456, 12345, 1234, 123, 12]

for i, num in enumerate(nums):
    if sum(number_to_list(num)) % 3 == 0:
        del nums[i]

max_prime = 0
for num in nums:
    for p in permutation(number_to_list(num)):
        if p[-1] not in [0, 2, 4, 6, 8]:
            test_num = list_to_number(p)
            if is_prime(test_num) and test_num > max_prime:
                max_prime = test_num
    if max_prime != 0:
        break

print(max_prime)
