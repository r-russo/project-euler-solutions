#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

int main() {
    std::fstream file;
    file.open("../p059_cipher.txt", std::fstream::in);
    std::string text, new_text;
    std::vector<std::string> dictionary = {
            "the ",
            "be ",
            "to ",
            "of ",
            "and",
            "in ",
            "tha",
            "hav",
            "it ",
            "for"
    };
    if (file.is_open()) {
        char c, tmp {0};
        while (file.get(c)) {
            if (c != ',') {
                tmp *= 10;
                tmp += c - '0';
            } else {
                text.push_back(tmp);
                tmp = 0;
            }
        }
        text.push_back(tmp);

        int count;
        int max_count {0};
        std::vector<char> best_pass(3);
        for (int i = 32; i <= 126; i++) {
            for (int j = 32; j <= 126; j++) {
                for (int k = 32; k <= 126; k++) {
                    count = 0;
                    new_text = text;
                    for (int pos = 0; pos < text.size(); pos = pos + 3) {
                        new_text.at(pos) ^= i;
                        new_text.at(pos + 1) ^= j;
                        new_text.at(pos + 2) ^= k;
//                        if (new_text[pos] >= 65 && new_text[pos] <= 91 ||
//                                new_text[pos] >= 97 && new_text[pos] <= 122) {
//                            count++;
//                        }
//
//                        if (new_text[pos + 1] >= 65 && new_text[pos + 1] <= 91 ||
//                                new_text[pos + 1] >= 97 && new_text[pos + 1] <= 122) {
//                            count++;
//                        }
//
//                        if (new_text[pos + 2] >= 65 && new_text[pos + 2] <= 91 ||
//                                new_text[pos + 2] >= 97 && new_text[pos + 2] <= 122) {
//                            count++;
//                        }

                        if (new_text[pos] < 32 && new_text[pos] != 13 && new_text[pos] != 10 ||
                                new_text[pos + 1] < 32 && new_text[pos + 1] != 13 && new_text[pos + 1] != 10 ||
                                new_text[pos + 2] < 32 && new_text[pos + 2] != 13 && new_text[pos + 2] != 10) {
                            break;
                        }

                        std::string substr = new_text.substr(pos, 3);
                        std::transform(substr.begin(), substr.end(), substr.begin(),
                                       [](unsigned char c){ return std::tolower(c); });
                        for (std::string &word: dictionary) {
                            if (substr == word) {
                                count++;
                            }
                        }
                    }

                    if (count > max_count) {
                        max_count = count;
                        best_pass[0] = i;
                        best_pass[1] = j;
                        best_pass[2] = k;
                        std::cout << count << ": " << i << " " << j << " " << k << std::endl;
                        std::cout << new_text.substr(0, 20) << std::endl;
                    }
                }
            }
        }

        for (int pos = 0; pos < text.size(); pos = pos + 3) {
            text.at(pos) ^= best_pass[0];
            text.at(pos + 1) ^= best_pass[1];
            text.at(pos + 2) ^= best_pass[2];
        }

        int sum {0};
        for (char pos : text) {
            sum += pos;
        }

        std::cout << sum << std::endl;
    }

    return 0;
}