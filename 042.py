from math import sqrt


def is_triangle_number(num):
    if num >= 1:
        test = sqrt(1 + 8 * num)
        return int(test) == test
    else:
        return False


# Read file

with open("p042_words.txt", 'r') as f:
    words = f.readlines()[0].replace('"', '').upper().split(',')

count = 0
for word in words:
    num = 0
    for char in word:
        num += ord(char) - 64
    if is_triangle_number(num):
        count += 1

print("%d triangle words in file" % count)
