#include<stdio.h>

typedef int bool;
#define true 1
#define false 0

int main() {
    int r;
    int num;
    int max_dec = 0;
    int c;
    for (int d = 2; d < 1000; d++) {
        int dec[1000];
        bool listo = false;
        int i = 0;
        c = 0;
        r = 1 % d;
        dec[0] = r;
        while (listo == false) {
            r = r * 10;
            r = r % d;
            if (r == 0)
                break;
            dec[++c] = r;
            for (i = 0; i < c; i++) {
                if (dec[i] == r) {
                    listo = true;
                    if (max_dec < c - i) {
                        max_dec = c - i;
                        num = d;
                    }
                    break;
                }
            }
        }
        printf("%d (%d). Max: %d (%d)\n", d, c - i, num, max_dec);
    }
    return 0;
}
