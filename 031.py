coins = [1, 2, 5, 10, 20, 50, 100, 200]

table = [[0 for j in range(len(coins))] for i in range(coins[-1] + 1)]
for i in range(coins[-1] + 1):
    table[i][0] = 1

for i in range(coins[-1] + 1):
    for j in range(1, len(coins)):
        table[i][j] = table[i][j - 1]

        if coins[j] <= i:
            table[i][j] += table[i - coins[j]][j]

print(table[-1][-1])
