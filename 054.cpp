#include <algorithm>
#include <fstream>
#include <iostream>
#include <numeric>
#include <vector>

const int score_one_pair = 1;
const int score_two_pairs = 2;
const int score_three_of_a_kind = 3;
const int score_straight = 4;
const int score_flush = 5;
const int score_full_house = 6;
const int score_four_of_a_kind = 7;
const int score_straight_flush = 8;
const int score_royal_flush = 9;

bool is_flush(std::vector<char> suit) {
    char first_suit = suit[0];
    bool res = true;
    for (int i = 1; i < suit.size(); i++) {
        if (suit[i] != first_suit) {
            res = false;
            break;
        }
    }

    return res;
}

bool is_straight(std::vector<int> value) {
    char last_val = value[0];
    bool res = true;
    for (int i = 1; i < value.size(); i++) {
        if (value[i] != last_val + 1) {
            res = false;
            break;
        }
        last_val = value[i];
    }

    return res;
}

void sort_cards(std::vector<int> &value, std::vector<char> &suit) {
    std::vector<size_t> ix(value.size());
    std::iota(ix.begin(), ix.end(), 0);
    std::sort(ix.begin(), ix.end(), [&](std::size_t i, std::size_t j) {
        return value[i] < value[j];
    });

    std::vector<int> new_value;
    std::vector<char> new_suit;

    new_value.reserve(value.size());
    new_suit.reserve(suit.size());

    for (unsigned long i : ix) {
        new_value.push_back(value[i]);
        new_suit.push_back(suit[i]);
    }

    value = new_value;
    suit = new_suit;
}

int get_score(const std::string &hand, int &max) {
    std::vector<int> value;
    std::vector<char> suit;
    std::string card;

    value.reserve(5);
    suit.reserve(5);

    int score = 0;

    for (int i = 0; i < 5; i++) {
        card = hand.substr(i * 3, i * 3 + 2);

        char val = card.at(0);

        if (std::isdigit(val)) {
            value.push_back(std::stoi(&card.at(0)));
        } else {
            switch (val) {
                case 'T':value.push_back(10);
                    break;
                case 'J':value.push_back(11);
                    break;
                case 'Q':value.push_back(12);
                    break;
                case 'K':value.push_back(13);
                    break;
                case 'A':value.push_back(14);
                    break;
                default:break;
            }
        }

        suit.push_back(card.at(1));
    }

    sort_cards(value, suit);

    bool flush = is_flush(suit);
    bool straight = is_straight(value);

    max = value[value.size() - 1];
    if (flush && straight) {
        if (value.at(0) == 10) {
            score = score_royal_flush;
        } else {
            score = score_straight_flush;
        }
    } else if (!flush && straight) {
        score = score_straight;
    } else if (flush) {
        score = score_flush;
    } else {
        int pairs = 0;
        int three = 0;
        int four = 0;

        int c = 1;
        int last_val = value[0];
        for (int i = 1; i < value.size(); i++) {
            if (value[i] == last_val) {
                c++;
            }
            if (i == value.size() - 1 || value[i] != last_val) {
                if (c == 2) {
                    pairs++;
                    max = last_val;
                } else if (c == 3) {
                    three++;
                    max = last_val;
                } else if (c == 4) {
                    four++;
                    max = last_val;
                    break;
                }

                c = 1;
            }
            last_val = value[i];
        }

        if (four > 0) {
            score = score_four_of_a_kind;
        } else if (three > 0) {
            if (pairs == 0) {
                score = score_three_of_a_kind;
            } else {
                score = score_full_house;
            }
        } else if (pairs > 0) {
            if (pairs == 1) {
                score = score_one_pair;
            } else {
                score = score_two_pairs;
            }
        }
    }

    return score;
}

int main(int argc, char const *argv[]) {
    std::ifstream file;
    std::string line, hand1, hand2;
    file.open("p054_poker.txt");

    if (file.is_open()) {
        int win = 0;
        while (std::getline(file, line)) {
            hand1 = line.substr(0, 14);
            hand2 = line.substr(15, 29);

            int max1, max2;

            int score1 = get_score(hand1, max1);
            int score2 = get_score(hand2, max2);

            if ((score1 > score2) || ((score1 == score2) && (max1 > max2))) {
                win++;
            }
        }
        std::cout << win << std::endl;
        file.close();
    }

    return 0;
}
