with open('p022_names.txt', 'r') as f:
    names = f.readlines()[0].replace('"', '').split(',')

names.sort()
start = ord('A') - 1
s = 0
for ix, n in enumerate(names):
    sx = 0
    for i in n:
        sx += ord(i) - start
    s += sx*(ix+1)

print(s)
