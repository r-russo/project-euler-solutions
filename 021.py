import line_profiler

from time import sleep
from math import sqrt

def prime(n):
    for i in range(2, int(sqrt(n)) + 1):
        if n % i == 0:
            return False
    return True

#@profile
def d(n, primes):
    s = 1
    for ix, p in enumerate(primes):
        # s = (1 + p1 + p1^2 ...)( ... ) - n
        sx = 1
        t = 0
        while True:
            if n % p == 0:
                t += 1
                n //= p
                sx += p**t
            else:
                s *= sx
                break

        if n == 1:
            break

    return s

def find_amic():
    p = []
    i = 2
    sum_amic = 0
    n_amic = 0
    sums = []
    while i < 10000:
        try:
            if prime(i):
                p.append(i)
                i += 1
                sums.append(1)
                continue
            s = d(i, p) - i
            sums.append(s)
            if s < i:
                if sums[s-2] == i and s == sums[i - 2]:
                    sum_amic += i + s
                    n_amic += 1
            i += 1
            print('(%d) Amicable numbers: %d' % (i, n_amic), end='\r')
        except KeyboardInterrupt:
            break
    print()
    return sum_amic

print(find_amic())
