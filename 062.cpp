#include <algorithm>
#include <cmath>
#include <iostream>
#include <string>
#include <unordered_map>

bool is_permutation(uint64_t n, uint64_t m) {
    std::string str_n = std::to_string(n);
    std::string str_m = std::to_string(m);

    if (str_n.size() != str_m.size()) {
        return false;
    }

    std::sort(str_n.begin(), str_n.end());
    std::sort(str_m.begin(), str_m.end());

    return str_n == str_m;
}

int main() {
    /* std::unordered_map<uint64_t, uint8_t> cubes_permutations; */

    /* uint64_t cube{}, result{}; */
    /* bool found_result{}; */
    /* for (uint64_t i = 340; i < 10000; i++) { */
    /*     std::cout << i << std::endl; */
    /*     cube = pow(i, 3); */

    /*     bool found_permutation{}; */
    /*     for (auto &el : cubes_permutations) { */
    /*         if (is_permutation(cube, el.first)) { */
    /*             el.second++; */
    /*             if (el.second == 5) { */
    /*                 result = el.first; */
    /*                 found_result = true; */
    /*                 break; */
    /*             } */
    /*             found_permutation = true; */
    /*         } */
    /*     } */

    /*     if (found_result) { */
    /*         break; */
    /*     } */

    /*     if (!found_permutation) { */
    /*         if (cubes_permutations.count(cube) == 0) { */
    /*             std::pair<uint64_t, uint8_t> el(cube, 1); */
    /*             cubes_permutations.insert(el); */
    /*         } */
    /*     } */
    /* } */

    /* if (found_result) { */
    /*     std::cout << result << std::endl; */
    /* } */

    uint64_t cube{}, upper_bound{}, max_cube{};
    for (uint64_t i = 340; i < 10000; i++) {
        cube = pow(i, 3);
        max_cube = pow(10, ceil(log10(cube)));
        upper_bound = floor(cbrt(max_cube));

        int count_permutations{0};
        bool found{};
        for (uint64_t j = i + 1; j < upper_bound; j++) {
            count_permutations += is_permutation(cube, pow(j, 3)) ? 1 : 0;

            if (count_permutations == 4) {
                found = true;
                std::cout << cube << std::endl;
                break;
            }
        }

        if (found) {
            break;
        }
    }

    return 0;
}
