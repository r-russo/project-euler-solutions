def heaps_algorithm(k, a, nums):
    if k == 1:
        nums.append(a.copy())
    else:
        heaps_algorithm(k-1, a, nums)

        for i in range(k-1):
            if k % 2 == 0:
                tmp = a[i]
                a[i] = a[k-1]
                a[k-1] = tmp
            else:
                tmp = a[0]
                a[0] = a[k-1]
                a[k-1] = tmp
            heaps_algorithm(k-1, a, nums)

def pandigital_gen(n=10):
    nums = []
    start = list(range(n))
    heaps_algorithm(len(start), start, nums)
    for i in range(len(nums)):
        nums[i] = "".join([str(x) for x in nums[i]])
    return nums

nums = pandigital_gen()

pan_sum = 0
divs = [2, 3, 5, 7, 11, 13, 17]

for num in nums:
    is_div = True
    for i, div in zip(range(1, 8), divs):
        d = int(num[i] + num[i+1] + num[i+2])
        if not d % div == 0:
            is_div = False
            break

    if is_div:
        pan_sum += int(num)

print(pan_sum)