#include <iostream>
#include "tools.h"

void sqrt2_expansion(int it, StrInt &num, StrInt &den) {
    num = StrInt(1);
    den = StrInt(2);
    StrInt tmp;
    for (int i = 0; i < it - 1; i++) {
        // 1/(2 + x/y)
        tmp = den;
        den = den * 2 + num;
        num = tmp;
    }

    num = den + num;
}

int main() {
    StrInt num, den;
    int d_num, d_den;
    int c {0};

    for (int i = 1; i <= 1000; i++) {
        sqrt2_expansion(i, num, den);
//        std::cout << num << "/" << den;
        d_num = num.num_digits();
        d_den = den.num_digits();
        if (d_num > d_den) {
            c++;
//            std::cout << " x";
        }
//        std::cout << std::endl;
    }

    std::cout << c << std::endl;

    return 0;
}