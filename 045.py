from math import sqrt


def is_triangular(n):
    return (-1 + sqrt(1 + 8 * n)) % 2 == 0


def is_pentagonal(n):
    return (1 + sqrt(1 + 24*n)) % 6 == 0


def is_hexagonal(n):
    return (1 + sqrt(1 + 8*n)) % 4 == 0


for n in range(2, int(1e8)):
    hex_num = n*(2*n - 1)
    if is_triangular(hex_num) and is_pentagonal(hex_num) and \
            is_hexagonal(hex_num):
        print(hex_num)
