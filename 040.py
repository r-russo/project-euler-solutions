def champernowne_digit(n):
    if n <= 0:
        return None
    if n <= 9:
        return n
    i = 1

    test_cond = 21
    while test_cond*9 < n:
        i += 1
        test_cond += (i+1)*10**i
    digit_ix = n - 9
    for j in range(1, i):
        digit_ix -= 9 * (1 + j) * 10**j

    num = (digit_ix - 1) // (i + 1) + 10**i
    ix = (digit_ix - 1) % (i + 1)
    return int(str(num)[ix])


assert(champernowne_digit(12) == 1)
a = 1
for i in range(0, 7):
    a *= champernowne_digit(10**i)

print(a)
