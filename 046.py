from tools import is_prime
from math import sqrt


n = 9
while True:
    found = False
    if not is_prime(n):
        for p in range(2, n):
            if found:
                break
            if is_prime(p):
                for s in range(1, int(sqrt(n - p)) + 1):
                    if p + 2*s**2 == n:
                        print("%d = %d + 2 × %d^2" % (n, p, s))
                        found = True
                        break
        if not found:
            print(n)
            break
    n += 2

