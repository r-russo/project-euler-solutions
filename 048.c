#include <math.h>
#include <stdio.h>

int count_digits(long long n);

int main() {
    const int max = 1000;
    const int total_digits = 10;

    long long result = 0;
    long long pot = pow(10, total_digits);

    for (int i = 1; i <= max; i++) {
        long long tmp = 1;
        for (int p = 0; p < i; p++) {
            tmp *= i;

            if (count_digits(tmp) > total_digits) {
                tmp -= tmp/pot * pot;
            }
        }
        result += tmp;
    }

    if (count_digits(result) > total_digits) {
        result -= result/pot * pot;
    }

    printf("1^1 + 2^2 + ... + %d^%d = %lli\n", max, max, result);
    return 0;
}

int count_digits(long long n) {
    int digits = 0;
    while (n > 0) {
        n /= 10;
        digits++;
    }

    return digits;
}
