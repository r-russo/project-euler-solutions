#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

int num_digits(long num) {
    return ceil(log10(num));
}

void extract_digits_and_sort(long num, std::vector<int> &digits) {
    int pos = 0;
    while (num != 0) {
        digits.push_back(num % 10);
        num /= 10;
        pos++;
    }

    std::sort(digits.begin(), digits.end());
}

bool test_digits(long num1, long num2) {
    int nd1 = num_digits(num1);
    int nd2 = num_digits(num2);
    if (nd1 != nd2) {
        return false;
    }

    std::vector<int> d1;
    std::vector<int> d2;

    extract_digits_and_sort(num1, d1);
    extract_digits_and_sort(num2, d2);

    return d1 == d2;
}

int main() {
    for (long num = 100; num < 100000000; num++) {
        if (! test_digits(num, num * 2)) {
            continue;
        }

        if (! test_digits(num, num * 3)) {
            continue;
        }

        if (! test_digits(num, num * 4)) {
            continue;
        }

        if (! test_digits(num, num * 5)) {
            continue;
        }

        if (! test_digits(num, num * 6)) {
            continue;
        }

        std::cout << num << std::endl;
        break;
    }

    return 0;
}