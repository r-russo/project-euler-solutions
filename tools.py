import math


def is_prime(n):
    if n == 1:
        return False
    if n == 2:
        return True
    for i in range(2, round(math.sqrt(n))+1):
        if n % i == 0:
            return False

    return True


def is_pandigital(n, start=1):
    digits = [int(i) for i in str(n)]
    if len(digits) <= 10 - start:
        for d in range(start, len(digits) - start):
            if d not in digits:
                return False

        return True

    return False


def eratosthenes(N):
    list_of_nums = range(int(N) + 1)
    primes = [True] * len(list_of_nums)

    for ix, n in enumerate(list_of_nums):
        if n < 2:
            primes[ix] = False
        if not primes[ix]:
            continue

        primes[ix + n::n] = map(lambda x: False, range(len(primes[ix + n::n])))

    return list(filter(lambda x: primes[x], list_of_nums))


def prime_factors(num, primes, powers=False):
    """prime_factors

    :param num: number
    :param primes: list of primes
    :param powers: if True return unique values of factors and a list of
    powers
    """
    factors = []
    if powers:
        list_powers = []

    if num < 2:
        return None

    if num in primes:
        if powers:
            return ([num], [1])
        else:
            return [num]

    while num != 1:
        for p in primes:
            while num % p == 0:
                num /= p
                if powers:
                    if p not in factors:
                        factors.append(p)
                        list_powers.append(1)
                    else:
                        list_powers[-1] += 1
                else:
                    factors.append(p)

    if powers:
        return factors, list_powers

    return factors

def heaps_algorithm(k, a, nums):
    if k == 1:
        nums.append(a.copy())
    else:
        heaps_algorithm(k-1, a, nums)

        for i in range(k-1):
            if k % 2 == 0:
                tmp = a[i]
                a[i] = a[k-1]
                a[k-1] = tmp
            else:
                tmp = a[0]
                a[0] = a[k-1]
                a[k-1] = tmp
            heaps_algorithm(k-1, a, nums)