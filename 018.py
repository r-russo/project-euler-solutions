def greatest_sum(T):
    for row in range(len(T) - 2, -1, -1):
        for col in range(len(T[row])):
            if T[row+1][col] > T[row+1][col + 1]:
                T[row][col] += T[row+1][col]
            else:
                T[row][col] += T[row+1][col + 1]
    return T[0]

with open('p067_triangle.txt', 'r') as f:
    T = []
    for line in f:
        vec = [int(i) for i in line.strip().split(' ')]
        T.append(vec)

print(T)
print(greatest_sum(T))
