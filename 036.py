def is_palindrome(n):
    digits = list(str(n))
    l = len(digits)
    palindrome = True
    for i in range(l//2):
        if digits[i] != digits[l-i-1]:
            palindrome = False
            break

    return palindrome

def dec2bin(n):
    return bin(n)[2:]

assert is_palindrome(585)
assert not is_palindrome(123)

s = 0
for i in range(1000000):
    if is_palindrome(i) and is_palindrome(dec2bin(i)):
        s += i
        print(i)

print(s)
