#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define size_table 1000000

long sieve_erathosthenes(long* table, size_t len);
char is_prime(long n, long* table, long n_primes);

int main(int argc, char *argv[])
{
    const long max_prime = 1e6;
    static long table[size_table];
    long n_primes = sieve_erathosthenes(table, size_table);

    for (int i = n_primes - 1; i >= 0; i--) {
        if (table[i] > 1e6) {
            n_primes--;
        } else {
            break;
        }
    }

    printf("Generated table of %li primes\n", n_primes);

    int start_ix = 0;
    int ix = 0;

    long sum = 0;
    int count = 0;
    int max_terms = 0;

    while(1) {
        sum += table[ix++];
        count++;

        if (is_prime(sum, table, n_primes) && count > max_terms) {
            printf("Found sum of consecutive primes %li with %d terms\n",\
                    sum, count);
            max_terms = count;
        }

        if (ix == n_primes || sum > max_prime) {
            start_ix++;
            ix = start_ix;
            sum = 0;
            count = 0;
        }

        if (start_ix == n_primes) {
            break;
        }
    }


    return 0;
}

long sieve_erathosthenes(long* table, size_t len) {
    char is_prime_table[len];
    for (int i = 0; i < len; i++) {
        is_prime_table[i] = 1;
    }

    long n_primes = 0;
    for (long i = 0; i < len; i++) {
        if (is_prime_table[i]) {
            table[n_primes++] = i + 2;

            for (long j = i; j < len; j = j + i + 2) {
                is_prime_table[j] = 0;
            }
        }
    }

    return n_primes;
}

char is_prime(long n, long* table, long n_primes) {
    for (long i = 0; i < n_primes; i++) {
        if (n == table[i]) {
            return 1;
        }
    }

    return 0;
}
