import math

def is_prime(n):
    for i in range(2, round(math.sqrt(n) + 1)):
        if n % i == 0:
            return False

    return True

def rotation(n):
    digits = [i for i in str(n)*2]
    l = len(digits)//2
    for i in range(l):
        yield int(''.join(digits[i:i+l]))


def check_circular_prime(n, list_circular_primes):
    if n not in list_circular_primes:
        if is_prime(n):
            circular = True
            rots = list(set(rotation(n)))
            for i in rots:
                if not is_prime(i):
                    circular = False
                    break
            if circular:
                list_circular_primes += rots

assert is_prime(2)
assert not is_prime(4)
assert is_prime(73)
assert list(rotation(197)) == [197, 971, 719]
assert list(rotation(4)) == [4]

list_circ = []
for i in range(2, 1000000):
    check_circular_prime(i, list_circ)

print(len(list_circ))
