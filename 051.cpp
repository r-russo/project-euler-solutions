#include <iostream>
#include <cmath>
#include <string>
#include <bitset>
#include "tools.h"

std::string replace_number_with_asterisk(std::string number, int digit) {
    size_t replace_pos = number.find("*");
    while (replace_pos != std::string::npos) {
        if (replace_pos == 0 && digit == 0) {
            return "nan";
        }
        number = number.replace(replace_pos, 1, std::to_string(digit));
        replace_pos = number.find("*");
    }

    return number;
}

int count_prime_families(std::string template_num) {
    std::string str_new_number;
    int c = 0;
    for (int new_digits = 0; new_digits < 10; new_digits++) {
        str_new_number = replace_number_with_asterisk(template_num, new_digits);
        if (str_new_number.compare("nan") == 0) {
            continue;
        }
        if (is_prime(std::stol(str_new_number))) {
            c++;
        }
    }

    return c;
}

int check_n_prime_families(std::string template_num, int n) {
    std::string str_new_number;
    int c = 0;
    for (int new_digits = 0; new_digits < 10; new_digits++) {
        if (new_digits - c > 10 - n) {
            return false;
        }
        str_new_number = replace_number_with_asterisk(template_num, new_digits);
        if (str_new_number.compare("nan") == 0) {
            continue;
        }
        if (is_prime(std::stol(str_new_number))) {
            c++;
        }
    }

    return c == n;
}

long smallest_prime_in_family(std::string template_num) {
    std::string str_new_number;
    int c = 0;
    for (int new_digits = 0; new_digits < 10; new_digits++) {
        str_new_number = replace_number_with_asterisk(template_num, new_digits);
        if (str_new_number.compare("nan") == 0) {
            continue;
        }
        if (is_prime(std::stol(str_new_number))) {
            break;
        }
    }

    return std::stol(str_new_number);
}

int main() {
    int prime_family_counter;
    bool finish = false;

    for (long number = 101; number < 10000011; number = number + 2) {
        if (finish) {
            break;
        }
        int num_digits = ceil(log10(number));
        for (unsigned int i = 1; i < (1 << (num_digits - 1)); i++) {
            std::string str_number = std::to_string(number);
            for (int d = 2; d <= num_digits; d++) {
                if ((i << 1) & (1 << (d - 1))){
                    str_number = str_number.replace(str_number.length() - d, 1, "*");
                }  
            }

            if (check_n_prime_families(str_number, 8)) {
                std::cout << count_prime_families(str_number) << " primes in " << str_number << std::endl;
                std::cout << smallest_prime_in_family(str_number) << " is the minimum" << std::endl;
                finish = true;
                break;
            }
        }    
    }
    
    return 0;
}