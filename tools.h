#ifndef _TOOLS_H_
#define _TOOLS_H_

#include <cmath>
#include <string>

bool is_prime(unsigned long long n);
unsigned long long palindrome(unsigned long long n);

class StrInt {
public:
    StrInt();
    explicit StrInt(int x);
    explicit StrInt(std::string x);
    StrInt operator+(StrInt a);
    StrInt operator*(StrInt a);
    StrInt operator*(int a);
    friend std::ostream& operator<<(std::ostream &out, const StrInt &a);
    int get_digit(int d);
    int num_digits();
private:
    std::string x;
};
#endif