def is_pandig(n):
    list_n = list(str(n))
    for i in range(1, len(list_n)+1):
        if not str(i) in list_n:
            return False
    return True

def count_digs(n):
    return len(list(str(n)))

# n x m = l
# 1 x 4 = 4

totals = []
l = 4
for n in range(1, 10):
    for m in range(1000, int(10**l/n)+1):
        total = n*m
        if is_pandig(str(n) + str(m) + str(total)):
            if total not in totals:
                totals.append(total)

# 2 x 3 = 4
for n in range(10, 100):
    for m in range(100, int(10**l/n)+1):
        total = n*m
        if is_pandig(str(n) + str(m) + str(total)):
            if total not in totals:
                totals.append(total)

print(sum(totals))

# Tests
assert is_pandig(12)
assert is_pandig(15234)
assert not is_pandig(5123)
assert is_pandig(123456789)
assert not is_pandig(111111111111)
assert not is_pandig(123456788)

assert count_digs(9*1111) == 4
assert count_digs(99*101) == 4
assert count_digs(19*int(10**4/19)) == 4
