unidades = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
especiales = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
decenas = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']

c = 0
tmp = ''

for i in range(10): # centenas
    if i > 0:
        tmp += unidades[i-1] + 'hundred'
    for j in range(10): # decenas
        if j >= 2:
            if i > 0:
                tmp += unidades[i-1] + 'hundredand'
            tmp += decenas[j-2]
            for k in range(9): # unidades
                #tmp += '\n'
                if i > 0:
                    tmp += unidades[i-1] + 'hundredand'
                tmp += decenas[j-2] + unidades[k]
        elif j == 0:
            for k in range(9):
                if i > 0:
                    tmp += unidades[i-1] + 'hundredand'
                tmp += unidades[k]
        elif j == 1:
            if i > 0:
                tmp += ''.join([unidades[i-1] + 'hundredand' + e for e in especiales])
            else:
                tmp += ''.join(especiales)

#c = len(''.join([i + 'hundredand' for i in unidades]))*99 + len(tmp)*10
#c += len(''.join([i + 'hundred' for i in unidades])) + len('onethousand')

print(tmp)
print(len(tmp) + len('onethousand'))

