from tools import is_prime, heaps_algorithm

for n in range(1000, 10000):
    if is_prime(n):
        num = list(str(n))
        str_permutations = []
        permutations = []
        heaps_algorithm(len(num), num, str_permutations)

        for p in str_permutations:
            x = int(''.join(p))
            if x >= 1000 and x not in permutations:
                if is_prime(x):
                    permutations.append(x)

        diffs = {}
        vals = {}
        permutations = sorted(permutations)
        for i in range(len(permutations)):
            for j in range(i+1, len(permutations)):
                for k in range(j+1, len(permutations)):
                    if permutations[k] - permutations[j] == permutations[j] - permutations[i]:
                        print(permutations[k]-permutations[j])
                        print(permutations[i], permutations[j], permutations[k])