from tools import is_prime

digits = 5

# generate digit masks
masks = []
for i in range(2**digits):
    mask = f"{i:0{digits}b}"
    bool_mask = []
    for b in mask:
        bool_mask.append(int(b))

    mask = []
    if sum(bool_mask) >= 2:
        for i, b in enumerate(bool_mask):
            if b:
                mask.append(i)

        masks.append(mask)

start = int(10**(digits-1))
end = int(10**digits)

primes = []

for num in range(start, end):
    if is_prime(num):
        primes.append(num)

for num in primes:
    num = 56123

    # replace with every mask
    for mask in masks:
        count = 0
        n = list(str(num))
        # iterate through every digit
        for d in range(10):
            # apply mask
            for i in mask:
                n[i] = str(d)

            # test if is prime
            if int("".join(n)) in primes:
                count += 1

        if count > 1:
            print(num, mask, count)
    break

