#include <iostream>
#include "tools.h"

int sum_until_palindrome(unsigned long long num) {
    int it = 0;
    unsigned long long pal = palindrome(num);
    num += pal;
    pal = palindrome(num);
    it++;
    while (num != pal) {
        num += pal;
        pal = palindrome(num);
        it++;

        if (it > 50) {
            it = -1;
            break;
        }
    }

    return it;
}

int main() {
    int it, c {0};
    for (int i = 0; i < 10000; i++) {
        it = sum_until_palindrome(i);
        if (it == -1) {
            c++;
        }
    }

    std::cout << c << std::endl;

    return 0;
}