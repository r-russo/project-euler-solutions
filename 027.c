#include<stdio.h>
#include<math.h>

int is_prime(int n) {
    int prime = 1;
    if (n == 1 || n < 0)
        return 0;
    for (int i = 2; i <= ceil(sqrt(n)); i++) {
        if (n % i == 0) {
            prime = 0;
            break;
        }
    }
    return prime;
}

int main() {
    int max_a, max_b, max_primes;
    int n, p, c;
    max_primes = 0;
    printf("%d\n", is_prime(1327));
    for (int a = 0; a < 1000; a++) {
        for (int b = 0; b <= 1000; b++) {
            // a > 0; b > 0
            n = 0;
            c = 0;
            while(1) {
                p = n*n + a*n + b;
                if (is_prime(p) != 1)
                    break;
                c++;
                n++;
            }
            if (c > max_primes) {
                max_a = a;
                max_b = b;
                max_primes = c;
                printf("%d primos para a = %d y b = %d\n", c, a, b);
            }

            // a < 0; b > 0
            n = 0;
            c = 0;
            while(1) {
                p = n*n - a*n + b;
                if (is_prime(p) != 1)
                    break;
                c++;
                n++;
            }
            if (c > max_primes) {
                max_a = -a;
                max_b = b;
                max_primes = c;
                printf("%d primos para a = %d y b = %d\n", c, -a, b);
            }

            // a > 0; b < 0
            n = 0;
            c = 0;
            while(1) {
                p = n*n + a*n - b;
                if (is_prime(p) != 1)
                    break;
                c++;
                n++;
            }
            if (c > max_primes) {
                max_a = a;
                max_b = -b;
                max_primes = c;
                printf("%d primos para a = %d y b = %d\n", c, a, -b);
            }

            // a < 0; b < 0
            n = 0;
            c = 0;
            while(1) {
                p = n*n - a*n - b;
                if (is_prime(p) != 1)
                    break;
                c++;
                n++;
            }
            if (c >= max_primes) {
                max_a = -a;
                max_b = -b;
                max_primes = c;
                printf("%d primos para a = %d y b = %d\n", c, -a, -b);
            }
        }
    }
    printf("Producto de los máximos: %d\n", max_a*max_b);
    return 0;
}
