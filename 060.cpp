#include <iostream>
#include <vector>
#include <algorithm>
#include "tools.h"

bool check_pairs(const std::vector<std::string> &set, const std::string &np) {
    std::string p1, p2;
    if (set.empty()) {
        return true;
    }

    for (const auto &p: set) {
        p1 = np + p;
        p2 = p + np;
        if (!is_prime(std::stol(p1)) || !is_prime(std::stol(p2))) {
            return false;
        }
    }

    return true;
}

int rec_find_pairs(std::vector<std::string> &set, int next_ix, std::vector<uint64_t> primes, int depth) {
    if (depth == 0) {
        int sum = 0;
        for (const auto &p: set) {
            std::cout << p << " ";
            sum += std::stoi(p);
        }
        std::cout << std::endl;
        return sum;
    }

    if (next_ix == primes.size()) {
        std::string last_prime = set.back();
        set.pop_back();
        auto it = std::find(primes.begin(), primes.end(), std::stol(last_prime));
        next_ix = std::distance(primes.begin(), it) + 1;
        depth++;
    }

    std::string np = std::to_string(primes[next_ix]);
    if (check_pairs(set, np)) {
        set.push_back(np);
        return rec_find_pairs(set, next_ix + 1, primes, depth - 1);
    } else {
        return rec_find_pairs(set, next_ix + 1, primes, depth);
    }
}

int main() {
    std::vector<uint64_t> primes;

    const uint64_t max_prime = 50000;

    for (auto p = 3; p < max_prime; p++) {
        if (is_prime(p)) {
            primes.push_back(p);
        }
    }

    std::vector<std::string> set;
//    int ix = 3;
//    int sum = rec_find_pairs(set, ix++, primes, 5);
//    while (sum < 0) {
//        std::cout << primes[ix] << std::endl;
//        sum = rec_find_pairs(set, ix++, primes, 5);
//    }
//    std::cout << sum << std::endl;

    std::string np;
    for (int i = 2; i < primes.size(); i++) {
        std::cout << primes[i] << std::endl;
        set.clear();
        set.push_back(std::to_string(primes[i]));
        for (int j = i + 1; j < primes.size(); j++) {
            np = std::to_string(primes[j]);
            if (check_pairs(set, np)) {
                set.push_back(np);
                for (int k = j + 1; k < primes.size(); k++) {
                    np = std::to_string(primes[k]);
                    if (check_pairs(set, np)) {
                        set.push_back(np);
                        for (int l = k + 1; l < primes.size(); l++) {
                            np = std::to_string(primes[l]);
                            if (check_pairs(set, np)) {
                                set.push_back(np);
                                for (int m = l + 1; m < primes.size(); m++) {
                                    np = std::to_string(primes[m]);
                                    if (check_pairs(set, np)) {
                                        set.push_back(np);
                                        int sum = 0;
                                        for (const auto &p: set) {
                                            std::cout << p << " ";
                                            sum += std::stoi(p);
                                        }
                                        std::cout << ": " << sum << std::endl;
                                        set.pop_back();
                                    }
                                }
                                set.pop_back();
                            }
                        }
                        set.pop_back();
                    }
                }
                set.pop_back();
            }
        }
    }

    return 0;
}