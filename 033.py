def split_number(num):
    """Split two digit number in its digits"""
    first = num // 10
    second = num - first * 10
    return (first, second)


def cancel_and_compare(num, den):
    """Returns True if after canceling the fractions are equal and less than
    1"""
    if num == den:
        return False

    if num > den:
        return False

    digits_num = split_number(num)
    digits_den = split_number(den)

    # Test four posible cases
    if digits_num[0] == digits_den[0]:
        return num/den == digits_num[1] / digits_den[1]
    if digits_num[1] == digits_den[0]:
        return num/den == digits_num[0] / digits_den[1]
    if digits_num[0] == digits_den[1]:
        return num/den == digits_num[1] / digits_den[0]
    if digits_num[1] == digits_den[1]:
        return num/den == digits_num[0] / digits_den[0]


# Find the fractions which have repeated numbers in numerator and denominator
# that satisfy the condition

fractions = []
for num in range(11, 100):
    first_digit, second_digit = split_number(num)
    for i in range(1, 10):
        if cancel_and_compare(num, first_digit * 10 + i):
            fractions.append([num, first_digit * 10 + i])
        if i != first_digit:
            if cancel_and_compare(num, i * 10 + first_digit):
                fractions.append([num, i * 10 + first_digit])

        if first_digit != second_digit and second_digit != 0:
            if cancel_and_compare(num, second_digit * 10 + i):
                fractions.append([num, second_digit * 10 + i])
            if i != second_digit:
                if cancel_and_compare(num, i * 10 + second_digit):
                    fractions.append([num, i * 10 + second_digit])

print(fractions)
