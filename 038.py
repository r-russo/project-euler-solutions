from tools import is_pandigital

def concatenated_product(num, n):
    r = []
    for i in range(1, n+1):
        r.append(num * i)

    r = [str(i) for i in r]

    return ''.join(r)

num = 2
while num < 1e5:
    n = 2
    while True:
        r = concatenated_product(num, n)
        if len(r) == 9 and is_pandigital(r):
            print(num, r)
        elif len(r) > 9:
            break
        n += 1

    num += 1

