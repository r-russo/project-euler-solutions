def dow_per_year(year, start):
    day_of_the_month = [start]
    day = start

    # Feb
    day += 31
    dow = day % 7
    day_of_the_month.append(dow)

    # Mar
    day += 28
    if (year % 4 == 0 and not year % 100 == 0) or year % 400 == 0:
        day += 1
        print('Adding leap day to February %d' % year)
    dow = day % 7
    day_of_the_month.append(dow)

    # Apr
    day += 31
    dow = day % 7
    day_of_the_month.append(dow)

    # May
    day += 30
    dow = day % 7
    day_of_the_month.append(dow)

    # Jun
    day += 31
    dow = day % 7
    day_of_the_month.append(dow)

    # Jul
    day += 30
    dow = day % 7
    day_of_the_month.append(dow)

    # Aug
    day += 31
    dow = day % 7
    day_of_the_month.append(dow)

    # Sep
    day += 31
    dow = day % 7
    day_of_the_month.append(dow)

    # Oct
    day += 30
    dow = day % 7
    day_of_the_month.append(dow)

    # Nov
    day += 31
    dow = day % 7
    day_of_the_month.append(dow)

    # Dec
    day += 30
    dow = day % 7
    day_of_the_month.append(dow)

    # Jan
    day += 31
    next_start = day % 7

    return day_of_the_month, next_start


_, start = dow_per_year(1900, 1)
sundays = 0

for year in range(1901, 2001):
    day_of_the_month, start = dow_per_year(year, start)
    sundays += len(list(filter(lambda x: x == 0, day_of_the_month)))

print(sundays)
