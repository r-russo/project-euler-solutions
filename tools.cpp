#include "tools.h"

#include <utility>

bool is_prime(unsigned long long n) {
    if (n == 1 || n == 0) {
        return false;
    } else if (n == 2) {
        return true;
    } else {
        for (unsigned long long i = 2; i < round(sqrt(n)) + 1; i++) {
            if (n % i == 0) {
                return false;
            }
        }
    }

    return true;
}

unsigned long long palindrome(unsigned long long n) {
    unsigned long long new_n = 0;
    while (n != 0) {
        new_n *= 10;
        new_n += n % 10;
        n /= 10;
    }

    return new_n;
}

StrInt::StrInt() {
    this->x = std::to_string(0);
}

StrInt::StrInt(int x) {
    this->x = std::to_string(x);
}

StrInt::StrInt(std::string x) {
    this->x = std::move(x);
}

StrInt StrInt::operator+(StrInt a) {
    std::string result;
    int tmp, carry {0};
    int max = std::max(a.num_digits(), this->num_digits());
    for (auto i = 0; i < max; i++) {
        tmp = a.get_digit(a.num_digits() - i - 1) + this->get_digit(this->num_digits() - i - 1) + carry;
        carry = 0;
        if (tmp > 9) {
            carry += tmp / 10;
            tmp %= 10;
        }
        result.insert(0, 1, (char) (tmp + '0'));
    }

    if (carry > 0) {
        while (carry > 0) {
            result.insert(0, 1, (char) (carry % 10 + '0'));
            carry /= 10;
        }
    }
    return StrInt(result);
}

StrInt StrInt::operator*(StrInt a) {
//    std::string result;
//    int tmp, carry {0};
//    for (auto i = a.num_digits(); i > 0; i--) {
//        for (auto j = this->num_digits(); j > 0; j--) {
//            tmp = this->get_digit(j - 1) * a.get_digit(i - 1) + carry;
//            carry = 0;
//            if (tmp > 9) {
//                carry += tmp / 10;
//                tmp %= 10;
//            }
//            result.insert(0, 1, (char) (tmp + '0'));
//        }
//        if (carry > 0) {
//            while (carry > 0) {
//                result.insert(0, 1, (char) (carry % 10 + '0'));
//                carry /= 10;
//            }
//        }
//    }
//
//    return result;
}

StrInt StrInt::operator*(int a) {
    std::string result;
    int tmp, carry {0};
    for (auto i = this->num_digits(); i > 0; i--) {
        tmp = this->get_digit(i - 1) * a + carry;
        carry = 0;
        if (tmp > 9) {
            carry += tmp / 10;
            tmp %= 10;
        }
        result.insert(0, 1, (char) (tmp + '0'));
    }
    if (carry > 0) {
        while (carry > 0) {
            result.insert(0, 1, (char) (carry % 10 + '0'));
            carry /= 10;
        }
    }

    return StrInt(result);
}

int StrInt::get_digit(int d) {
    if (d < 0) {
        return 0;
    }

    return (int) (x.at(d) - '0');
}

int StrInt::num_digits() {
    return x.size();
}

std::ostream &operator<<(std::ostream &out, const StrInt &a) {
    out << a.x;
}
