#include<stdio.h>
#include<math.h>

double fi = (1 + sqrt(5))/2.0;
double psi = (1 - sqrt(5))/2.0;

double fib(int n) {
    return (pow(fi, n) - pow(psi, n))/sqrt(5);
}

int main(){
    printf("%i\n", fib(500));
    return 0;
}
