#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

int polygonal_number(int s, int n) {
    switch (s) {
        case 3: return n * (n + 1) / 2;
        case 4: return n * n;
        case 5: return n * (3 * n - 1) / 2;
        case 6: return n * (2 * n - 1);
        case 7: return n * (5 * n - 3) / 2;
        case 8: return n * (3 * n - 2);
        default: return 0;
    }
}

int inverse_polygonal_number(int s, int x) {
    switch (s) {
        case 3: return ceil((-1 + ceil(sqrt(1 + 8 * x))) / 2);
        case 4: return ceil(sqrt(x));
        case 5: return ceil((1 + sqrt(1 + 24 * x)) / 6);
        case 6: return ceil((1 + sqrt(1 + 8 * x)) / 4);
        case 7: return ceil((3 + sqrt(9 + 40 * x)) / 10);
        case 8: return ceil((2 + sqrt(4 + 12 * x)) / 6);
        default: return 0;
    }
}

int main() {
    int start = inverse_polygonal_number(3, 1000);
    int end = inverse_polygonal_number(3, 10000) - 1;

    std::vector<int> result, permutations;

    for (int s = 4; s <= 8; s++) {
        permutations.push_back(s);
    }
    for (int i = start; i <= end; i++) {
        do {
            result.clear();
            int num = polygonal_number(3, i);
            result.push_back(num);
            for (const int &p : permutations) {
                int digits = result.back() % 100;
                if (digits < 10) {
                    break;
                }
                int min = inverse_polygonal_number(p, digits * 100);
                int max = inverse_polygonal_number(p, (digits + 1) * 100);

                if (min == max) {
                    break;
                }

                result.push_back(polygonal_number(p, min));
            }

            if (result.size() == permutations.size() + 1) {
                int digits = result.back() % 100;
                int min = inverse_polygonal_number(3, digits * 100);
                int max = inverse_polygonal_number(3, (digits + 1) * 100);
                if (min < max && polygonal_number(3, min) == result.front()) {
                    int sum = 0;
                    for (const int &r : result) {
                        sum += r;
                    }
                    std::cout << sum << std::endl;
                }

            }
        } while (std::next_permutation(permutations.begin(), permutations.end()));
    }

    return 0;
}
