#include <iostream>
#include <string>
#include <cmath>

int sum_of_digits(const std::string &x) {
    int sum = 0;

    for (char i : x) {
        if (!isdigit(i)) {
            break;
        }
        sum += (int) (i - '0');
    }

    return sum;
}

std::string pow(int a, int b) {
    std::string result = std::to_string(1);
    int tmp, d, carry;
    for (int i = 0; i < b; i++) {
        carry = 0;
        for (char & p : result) {
            tmp = a * (int) (p - '0');
            d = tmp % 10 + carry;
            carry = tmp / 10;
            while (d > 9) {
                carry += d / 10;
                d = d % 10 ;
            }
            p = (char) (d + '0');
        }
        if (carry > 0) {
            while (carry > 9) {
                int c = carry % 10;
                result.push_back(c + '0');
                carry /= 10;
            }
            result.push_back(carry + '0');
        }
    }

    return result;
}

int main() {
    int d, max {0}, sum;
    std::string x;

    for (int a = 90; a < 100; a++) {
        for (int b = 90; b < 100; b++) {
            x = pow(a, b);
            sum = sum_of_digits(x);

            if (sum > max) {
                max = sum;
            }
        }
    }

    std::cout << max << std::endl;
    return 0;
}