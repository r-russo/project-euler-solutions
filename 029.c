#include<stdio.h>
#include<math.h>
#include<stdlib.h>

int compare(const void * a, const void * b) {
    if (*(double*)a > *(double*)b)
        return 1;
    else
        if (*(double*)a < *(double*)b)
            return -1;
    else
        return 0;
}

int main() {
    int n = 100;
    int m = (n -1) * (n - 1);
    double nums[m];
    int ix;
    for (int i = 2; i <= n; i++) {
        for (int j = 2; j <=n; j++) {
            ix = (i - 2)*(n - 1) + (j - 2);
            nums[ix] = pow(i, j);
        }
    }

    qsort(nums, m, sizeof(double), compare);

    int c = 0;
    int i = 0;
    while (1) {
        if (i == 0)
            c++;
        else {
            if (nums[i] != nums[i - 1])
                c++;
        }
        if (i == m-1) {
            break;
        }
        i++;
    }
    printf("%d\n", c);
    return 0;
}

