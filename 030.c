#include<stdio.h>
#include<math.h>

int suma_potencias(int n, int p) {
    int s = 0;
    while(1) {
        s += pow(n % 10, p);
        n = n / 10;
        if ( n == 0 )
            break;
    }
    return s;
}

int main() {
    // Tengo que calcular todas las sumas hasta que la suma de las potencias
    // supere a la cantidad de cifras
    int cifras = 1;
    int n = 1;
    int max_cifra;  // el máximo posible por cada cifra (la potencia de un número mayor excede
                    // cualquier número con la cantidad de cifras dadas
    int p = 6;  // potencia
    int s = 0;
    for(int cifras = 1; cifras <= round(p+log10(p)); cifras++) {
        // calcular el máximo por cifra
        for (max_cifra=1; max_cifra <= 9; max_cifra++) {
            if ( pow(max_cifra, p) > pow(10, cifras) ) {
                max_cifra--;
                break;
            }
        }
    }

    int tmp;
    for (int i = 2; i <= pow(10, round(p+log10(p))); i++) {
        tmp = suma_potencias(i, p);
        if (tmp == i) {
            s += tmp;
            printf("%d\n", i);
        }
    }

    printf("Suma: %d\n", s);
    return 0;
}
