max_sol = 0
max_p = 0
for p in range(1001):
    n_sol = 0
    for a in range(1, p-1):
        b = p/2*(p-2*a)/(p-a)
        if b == int(b) and b > 0:
            b = int(b)
            c = p - a - b
            n_sol += 1
    if n_sol > max_sol:
        max_sol = n_sol
        max_p = p
        print(max_sol, max_p)

