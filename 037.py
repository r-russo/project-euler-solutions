from math import log10
from tools import is_prime

assert is_prime(2)
assert is_prime(3)
assert is_prime(29)
assert not is_prime(12)
assert not is_prime(25)

primes = []
for i in range(11, 1000000, 2):
    if is_prime(i):
        n_digs = int(log10(i))
        trunprime = True
        n = i // 10
        for j in range(n_digs):
            if not is_prime(n):
                trunprime = False
                break
            n //= 10

        for j in range(n_digs):
            if not trunprime:
                break
            n = int(str(i)[j+1:])
            if not is_prime(n):
                trunprime = False
                break

        if trunprime:
            print(i)
            primes.append(i)

print()
print(len(primes))
print(sum(primes))
