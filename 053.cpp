#include <iostream>

double factorial(int num) {
    double r = 1;
    for (double i = 2; i <= num; i++) {
        r *= i;
    }

    return r;
}

double combination(int n, int k) {
    return factorial(n)/(factorial(k) * factorial(n - k));
}

int main(int argc, char const *argv[]) {
    double r;
    int c = 0;
    for (int i = 1; i <= 100; i++) {
        for (int j = 2; j < i; j++) {
            r = combination(i, j);
            if (r > 1000000) {
                c++;
            }
        }
    }
    
    std::cout << c << std::endl;

    return 0;
}
