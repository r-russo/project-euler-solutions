N = 1000000
list_of_nums = range(int(N))
consecutive_primes = [0] * len(list_of_nums)
consecutive = 4

for n in list_of_nums:
    if n < 2:
        consecutive_primes[n] = 0
        continue

    if consecutive_primes[n] == 0:
        for i in range(n, N, n):
            consecutive_primes[i] += 1
    else:
        continue

count = 0
for n in list_of_nums:
    if consecutive_primes[n] == consecutive:
        count += 1
        if count == consecutive:
            print(list(range(n - consecutive + 1, n + 1)))
            break
    else:
        count = 0
