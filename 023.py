import line_profiler
from time import sleep
from math import sqrt

def prime(n):
    for i in range(2, int(sqrt(n)) + 1):
        if n % i == 0:
            return False
    return True

#@profile
def d(n, primes):
    s = 1
    for ix, p in enumerate(primes):
        # s = (1 + p1 + p1^2 ...)( ... ) - n
        sx = 1
        t = 0
        while True:
            if n % p == 0:
                t += 1
                n //= p
                sx += p**t
            else:
                s *= sx
                break

        if n == 1:
            break

    return s

#@profile
def find_sum():
    s = 1
    ab = []
    p = []
    for i in range(2, 29000):
        sum_two_ab = False
        if prime(i):
            p.append(i)
        sd = d(i, p) - i
        if sd > i:
            ab.append(i)
        for x in ab:
            if (i - x) in ab:
                sum_two_ab = True
                break
        print('(%d/%d) Sum: %d' % (i, 28123, s), end='\r')
        if sum_two_ab:
            continue
        else:
            s += i
    print()
    return s

print(find_sum())
