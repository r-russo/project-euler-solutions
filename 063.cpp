#include <iostream>
#include <cmath>

bool is_powerful_digit(uint32_t m, uint32_t n) {
    float nlogm = n * log10(m);
    return nlogm < n && nlogm >= n - 1;
}

int main() {
    int n {}, m {1};
    int c {}, new_count {};
    do {
        n++;
        new_count = 0;
        while(n * log10(m) < n) {
            new_count += static_cast<uint32_t>(is_powerful_digit(m, n));
            m++;
        }
        c += new_count;
        m = 1;
    } while(new_count > 0);
    std::cout << c << std::endl;

    return 0;
}
