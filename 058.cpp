#include <iostream>
#include "tools.h"

#define ull unsigned long long

int main() {
    int dim {1}, c {0};
    double ratio {1};
    while (ratio > 0.1) {
        ull k, sq;
        k = 2 * (dim - dim / 2) + 1;
        sq = k * k;

        for (int j = 1; j <= 3; j++) {
            ull num = sq - j * (k - 1);

            if (is_prime(num)) {
                c++;
            }
        }
        ratio = static_cast<double>(c) / (2 * dim - 1);

        dim += 2;
    }

    std::cout << dim - 2 << std::endl;

    return 0;
}