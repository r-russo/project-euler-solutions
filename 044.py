from math import sqrt, inf


def pentagonal_number(num):
    return int(num*(3*num - 1)/2)


def is_pentagonal_number(num):
    test = (1 + sqrt(1 + 24*num))/6
    return int(test) == test


min_d = inf
for i in range(1, 100000):
    for j in range(i, 100000):
        if i == j:
            continue
        pi = pentagonal_number(i)
        pj = pentagonal_number(j)
        delta = abs(pi - pj)
        if is_pentagonal_number(pi + pj) and \
                is_pentagonal_number(delta):
            print(pi, pj)
            if min_d > delta:
                min_d = delta
                print("New minimum: %d" % min_d)

print(min_d)
