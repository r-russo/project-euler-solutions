#include<stdio.h>

int main() {
    int n = 1001;
    int c = 1;
    int sum = 1;
    while (1) {
        sum += (c*2-1)*(c*2-1)*4 + (c*2 * 1 + c*2 * 2 + c*2 * 3 + c*2 * 4);
        c++;
        if (c*2 - 1 >= n)
            break;
    }
    printf("%d\n", sum);
    return 0;
}
